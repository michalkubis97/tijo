package pl.edu.pwsztar.exceptions;

public class MovieNotFound extends RuntimeException{
    public MovieNotFound(String message) {
        super(message);
    }
}
