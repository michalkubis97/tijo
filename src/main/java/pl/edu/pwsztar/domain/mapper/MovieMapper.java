package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
 public class MovieMapper implements Converter<CreateMovieDto, Movie> {
    Converter<CreateMovieDto, Movie> convertToEntity = (CreateMovieDto createMovieDto) -> {
       return new Movie(createMovieDto.getTitle(),createMovieDto.getImage(),createMovieDto.getYear());

    };


    @Override
    public Movie convert(CreateMovieDto from) {
        return convertToEntity.convert(from);
    }
}
